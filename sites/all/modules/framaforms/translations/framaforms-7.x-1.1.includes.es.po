# $Id$
#
# LANGUAGE translation of Drupal (sites-all-modules-framaforms-includes)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  sites/all/modules/framaforms/includes/framaforms.admin.inc: n/a
#  sites/all/modules/framaforms/includes/framaforms.block.inc: n/a
#  sites/all/modules/framaforms/includes/framaforms.expiration.inc: n/a
#  sites/all/modules/framaforms/includes/framaforms.pages.inc: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-10-08 16:15+0200\n"
"PO-Revision-Date: 2021-04-24 07:15+0000\n"
"Last-Translator: Berto Te <ateira@3fpj.com>\n"
"Language-Team: Spanish <https://weblate.framasoft.org/projects/framaforms/"
"framaforms-include/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.5.1\n"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:20
msgid "Total number of forms : @number"
msgstr "Total de formularios: @number"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:30
msgid "Last @interval : @results"
msgstr "Último @interval : @results"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:39
msgid "Total number of submissions : @number"
msgstr "Total de respuestas: @number"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:40
msgid "(For an average of @number submissions / form)"
msgstr "(Con un promedio de @number respuestas / formulario)"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:47
msgid "Total number of users : @number"
msgstr "Total de usuaries: @number"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:57
msgid "Registered since @interval : @number"
msgstr "Registrades desde @interval : @number"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:63
msgid "Forms with the most submissions :"
msgstr "Formularios con la mayor cantidad de respuestas:"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:75
msgid "submissions"
msgstr "respuestas"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:76
msgid "Visit"
msgstr "Visitar"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:82
msgid "Users with the most forms"
msgstr "Usuaries con la mayor cantidad de formularios"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:103
msgid "Size of database :  @size"
msgstr "Tamaño de la base de datos: @size"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:119
msgid "General website information"
msgstr "Información general del sitio"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:123
msgid ""
"Note : some of these values can be changed elsewhere on the website, they "
"are simply centralized here."
msgstr ""
"Nota: algunos de estos valores se pueden cambiar en otras partes del sitio, "
"simplemente se centralizan aquí."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:127
msgid "Website title"
msgstr "Título del sitio"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:129
msgid "The title of this website. It will be displayed in the top header."
msgstr "El título del sitio. Será mostrado en el encabezado superior."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:134
msgid "Website slogan"
msgstr "Eslogan del sitio"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:136
msgid "Small phrase displayed under the title. Ex : 'Create forms easily'"
msgstr ""
"Frase corta dispuesta debajo del título. Ej: \"Crea formularios fácilmente\""

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:141
msgid "General contact email"
msgstr "Email de contacto general"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:143
msgid ""
"It will be used in the general contact form of the website, and used to warn "
"you of pending updates."
msgstr ""
"Será utilizado en el formulario de contacto general del sitio y se usará "
"para avisarte de actualizaciones pendientes."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:148
msgid "Link to support"
msgstr "Vínculo a página de ayuda"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:150
msgid ""
"Link to a support page for your users. This will be displayed when if your "
"users encounter problems creating forms."
msgstr ""
"Vínculo a una página de ayuda para tus usuaries. Éste se mostrará para "
"ayudar a tus usuaries a crear un formulario."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:155
msgid "Limitations"
msgstr "Limitaciones"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:159
msgid "Maximum number of form per user"
msgstr "Número máximo de formularios por usuarie"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:167
msgid "Forms expiration"
msgstr "Vencimiento de formularios"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:171
msgid "Expiration period (in weeks)"
msgstr "Intervalo de caducación (en semanas)"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:175
msgid ""
"When a user creates a form, its expiration date will be set by default at "
"this interval in the futur. Ex : if a user creates a form on 01/01/2020 and "
"the expiration value is set to '5', the form will expire by default on "
"02/05/2020. Insert '0' if you don't want forms to expire."
msgstr ""
"Cuando une usuarie crea un formulario, la fecha de vencimiento será seteada "
"por defecto según este intervalo. Ej: si crea un formulario el 01/01/2020 y "
"su valor de caducación está puesto en '5', el formulario se vencerá por "
"defecto el 05/02/2020. Ingresa '0' si no quieres que los formularios "
"caduquen."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:180
msgid "Deletion period (in weeks)"
msgstr "Intervalo de eliminación (en semanas)"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:184
msgid ""
"Interval of time between the form expiration and its deletion from the "
"database. Insert '0' if you don't want forms to be deleted."
msgstr ""
"Intervalo de tiempo entre el vencimiento del formulario y su eliminación de "
"la base de datos. Ingresa '0' si no quieres que los formularios sean "
"eliminados."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:189
msgid "Notification email"
msgstr "Email de notificación"

msgid "Save form submissions before deleting"
msgstr "Guarde los envíos de formularios antes de eliminarlos"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:191
msgid ""
"Check this box if you want all form submissions to be saved before deletion. "
"This can be useful for user support."
msgstr ""
"Marque esta casilla si desea que todos los envíos de formularios se guarden "
"antes de eliminarlos. Esto puede resultar útil para la asistencia al usuario."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:196
msgid "Generate default pages"
msgstr "Generar páginas por defecto"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:197
msgid ""
"Here you can change the content of the email that will be sent to the user "
"warning them of expired forms. <i>You can use the following tokens that will "
"be replaced : <strong>[form_url]</strong> (the form URL), "
"<strong>[form_expires_on]</strong> (the form's expiration date), "
"<strong>[form_title]</strong> (the form's title).</i>"
msgstr ""
"Aquí puede cambiar el contenido del correo electrónico que se enviará al "
"usuario advirtiéndole de los formularios caducados.<i>Puede usar los "
"siguientes tokens que serán reemplazados:<strong>[form_url]</strong> (la URL "
"del formulario), <strong>[form_expires_on</strong> (la fecha de vencimiento "
"del formulario), <strong>[form_title]</strong> (el título del "
"formulario).</i>"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:203
msgid "Reset all"
msgstr "Resetear todo"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:204
msgid "Reset all global variables to their initial value."
msgstr "Resetear todas las variables globales a su valor inicial."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:204
msgid ""
"By clicking this button, you will create default pages for your website : "
"homepage, 404/403 error pages, etc. You can modify them afterwards."
msgstr ""
"Al hacer clic en este botón, creará páginas predeterminadas para su sitio "
"web: página de inicio, páginas de error 404/403, etc. Puede modificarlas "
"posteriormente."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:217
msgid "Submit"
msgstr "Enviar"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:231
msgid "The default pages were created."
msgstr "Las páginas por defecto han sido creadas."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:243;247;251
msgid "There was an error in the form values."
msgstr "Hubo un error en los valores del formulario."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:244
msgid "The deletion period must be a number."
msgstr "El intervalo de eliminación debe ser un número."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:248
msgid "The expiration period must be a number."
msgstr "El intervalo de caducación debe ser un número."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:252
msgid "The number of form per user must be a number."
msgstr "El número de formularios por usuarie debe ser un número."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:260
msgid "The following directory doesn't exist or is not writable : !path."
msgstr "El siguiente directorio no existe o no se puede escribir: !path."

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:273
msgid "The form was submitted !"
msgstr "¡El formulario ha sido enviado!"

#: sites/all/modules/framaforms/includes/framaforms.admin.inc:286
msgid "All global variables for Framaforms were reset."
msgstr "Todas las variables globales de Framaforms han sido reseteadas."

#: sites/all/modules/framaforms/includes/framaforms.block.inc:31
msgid "Contact the author of this form"
msgstr "Contactar a le autore de este formulario"

#: sites/all/modules/framaforms/includes/framaforms.block.inc:32
msgid "To contact the author of this form, <a href='@link'> click here</a>"
msgstr ""
"Para contactar a le autore de este formulario, <a href='@link'> haz click "
"aquí</a>"

#: sites/all/modules/framaforms/includes/framaforms.block.inc:37
msgid ""
"Error in the contact form block (modules/framaforms/includes/framaforms."
"block.inc) : %error"
msgstr ""
"Error en el bloque del formulario de contactot (modules/framaforms/includes/"
"framaforms.block.inc) : %error"

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:248
msgid "Node !nid was not found and could not be exported."
msgstr "Nodo !nid no se encontró y no se pudo exportar."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:284
msgid "Submissions for node !nid exported."
msgstr "Opciones para el nodo !nid exportadas."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:42
msgid "There was an error inserting expired forms into framaforms_expired : %error"
msgstr "Error al insertar formularios vencidos a framaforms_expired : %error"

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:59
msgid "Notifying users of their expired forms."
msgstr "Notificación a les usuaries acerca de sus formularios vencidos."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:73
msgid ""
"Error while trying to notify the user : the user's email could be badly "
"configured."
msgstr ""
"Error al intentar notificar a usuarie: el email de le usuarie podría estar "
"mal configurado."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:75
msgid "Notified all users of their expired forms."
msgstr ""
"Todes les usuaries fueron notificades acerca de sus formularios vencidos."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:79
msgid "There was an error notifying users : %error"
msgstr "Error al notificar a los usuarios: %error"

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:102
msgid "Invalid email for user %uid"
msgstr "Email inválido para el usuario %uid"

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:148
msgid ""
"Removing forms from the framaforms_expired table after their expiration date "
"was pushed back."
msgstr ""
"La eliminación de formularios de la tabla framaforms_expired después de su "
"fecha de vencimiento se retrasó."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:173
msgid "Error updating modified nodes : %error"
msgstr "Error al modificar formularios: %error"

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:204
msgid "Deleting forms after notification."
msgstr "Eliminación de formularios después de la notificación."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:226
msgid "The following forms were deleted : %nodes"
msgstr "Los formularios siguientes fueron eliminados: %nodes"

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:229
msgid "There is no form to delete."
msgstr "No hay formularios para borrar."

#: sites/all/modules/framaforms/includes/framaforms.expiration.inc:233
msgid "Error deleting expired nodes : %error"
msgstr "Error eliminando los formularios caducados: %error"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:746
msgid ""
"<p>If you select 'yes', all results for this form will be accessible "
"<strong>by everyone</strong>.</p> Even if no one but you will be able to "
"edit the results, this is <strong>NOT RECOMMANDED</strong> because you might "
"make public private information of people who would not want it to be public "
"(first and last names, email and postal addresses, telephone numbers...)</"
"p>\n"
"<p><strong>By selecting 'yes', you agree to protect the anonymity of the "
"participants, and will be wholly responsible for information you make public."
"</strong></p>\n"
"<p>Please note that the 'Users having access to results' option below allows "
"you to give access to the results (read, modify and delete) to selected "
"authenticated users.</p>"
msgstr ""
"<p>Si selecciona \"sí\", todos los resultados de este formulario "
"serán<strong accesibles para todos</strong>.</p> Incluso si nadie más que "
"usted podrá editar los resultados, esto es <strong> NO RECOMENDADO </"
"strong>porque puede hacer pública la información privada de personas que no "
"quieren que sea pública (nombres y apellidos, direcciones de correo "
"electrónico y postales, números de teléfono ...) </p> \n"
"<p> <strong> Al seleccionar \"sí\", acepta proteger el anonimato de los "
"participantes y será totalmente responsable de la información que haga "
"pública. </strong> </p>\n"
"<p> Tenga en cuenta que la opción \"Usuarios con acceso a los resultados\" a "
"continuación le permite dar acceso a los resultados (leer, modificar y "
"eliminar) a los usuarios autenticados seleccionados. </p>"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:749
msgid "Public access to results"
msgstr "Acceso público a los resultados"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:750
msgid "Body"
msgstr "Cuerpo"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:751
msgid "Comment"
msgstr "Comentario"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:752
msgid "Expiration date"
msgstr "Fecha de caducidad"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:753
msgid "Description"
msgstr "Descripción"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:754
msgid "Template description"
msgstr "Descripción de la plantilla"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:755
msgid ""
"Describe what your form is about. This will appear above your form.<br/>\n"
"If you want to display pictures, you should consider using an external image "
"hosting service, like <a href='https://framapic.org' "
"target='_blank'>framapic.org</a>."
msgstr ""
"Describe de qué se trata tu formulario. Esto aparecerá encima de su "
"formulario.<br/>\n"
"Si desea mostrar imágenes, debe considerar el uso de un servicio de "
"alojamiento de imágenes externo, como<a href='https://framapic.org' "
"target='_blank'>framapic.org</a>."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:757
msgid "Attached files"
msgstr "Archivos adjuntos"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:758
msgid "Image"
msgstr "Imagen"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:759
msgid "Optional banner"
msgstr "Banner opcional"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:760
msgid ""
"Optional image that will be displayed at the top of your form (example : "
"logo or banner).<br/>\n"
"After having selected it by clicking 'Browse', don't forget to click "
"'Upload' to make it available online."
msgstr ""
"Imagen opcional que se mostrará en la parte superior de su formulario "
"(ejemplo: logotipo o banner).<br/>\n"
"Después de haberlo seleccionado haciendo clic en 'Examinar', no olvide hacer "
"clic en 'Cargar' para que esté disponible en línea."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:762
msgid ""
"Forms expire after 6 months, and are destroyed after 9 months (this cover "
"90% of use cases, and avoids database overloads).<br>\n"
"  At the given date, your form won't be accessible publically. You and you "
"only will however be able to access it - and its results - for 60 more days, "
"from your account.\n"
"  If you wish to extend its expiration date, you simply have to edit the "
"field above once your form is live.<br>\n"
"  If this constraint doesn't suit your needs or if you want more "
"information, please refer to <a href='/limitations'>the limitation page</"
"a>.\n"
"  "
msgstr ""
"Los formularios caducan después de 6 meses y se destruyen después de 9 meses "
"(esto cubre el 90% de los casos de uso y evita las sobrecargas de la base de "
"datos). <br>\n"
"En la fecha indicada, su formulario no será accesible públicamente. Sin "
"embargo, usted y solo podrán acceder a él, y a sus resultados, durante 60 "
"días más, desde su cuenta.\n"
"Si desea extender su fecha de vencimiento, simplemente tiene que editar el "
"campo de arriba una vez que su formulario esté activo. <br>\n"
"Si esta restricción no se adapta a sus necesidades o si desea obtener más "
"información, consulte <a href='/limitations'> la página de limitaciones </a>."
"\n"
"  "

#: sites/all/modules/framaforms/includes/framaforms.node.inc:767
msgid "List this form as a template"
msgstr "Incluya este formulario como plantilla"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:768
msgid ""
"A short description of your form template (for example : « Subscription form "
"for an concert »)."
msgstr ""
"Una breve descripción de su plantilla de formulario (por ejemplo: «"
"Formulario de suscripción para un concierto»)."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:769
msgid "Words separated by commas."
msgstr "Palabras separadas por comas."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:770
msgid ""
"If you check this box, you form will be displayed in the public list of "
"templates, allowing anybody to clone it, use it as a form template and adapt "
"it.<strong>If it is listed as a template, any visitor will be able to fill "
"in your form.</strong>."
msgstr ""
"Si marca esta casilla, su formulario se mostrará en la lista pública de "
"plantillas, lo que permitirá que cualquiera pueda clonarlo, utilizarlo como "
"plantilla de formulario y adaptarlo.<strong>Si aparece como una plantilla, "
"cualquier visitante podrá completar su formulario</strong>."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:771
msgid "Tags"
msgstr "Etiquetas"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:772
msgid "Upload an image to display with this content."
msgstr "Sube una imagen para mostrar con este contenido."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:773
msgid "Users having access to the results."
msgstr "Usuarios que tienen acceso a los resultados."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:774
msgid ""
"<p>You can authorize other authenticated users to access this form's "
"results : this requires them already having created an account. To do that, "
"fill in their usernames in the field above (separated by commas if several)."
"</p>\n"
"  <p>When logged in, these users will have access to the results by browsing "
"to the form's page, and clicking on the 'Results' tab. They will be able to "
"edit and delete results. <strong>They will however NOT be able to edit the "
"form itself</strong> (add or remove fields, change the title, etc).</p>\n"
"  <p>For more information, see <a href='https://docs.yakforms.org/features/partage-resultats/'>our "
"documentation</a>.</p>"
msgstr ""
"<p>Puede autorizar a otros usuarios autenticados a acceder a los resultados "
"de este formulario: esto requiere que ya hayan creado una cuenta. Para hacer "
"eso, complete sus nombres de usuario en el campo de arriba (separados por "
"comas si son varios).</p>\n"
"<p>Cuando inicien sesión, estos usuarios tendrán acceso a los resultados "
"navegando a la página del formulario y haciendo clic en la pestaña "
"'Resultados'. Podrán editar y eliminar resultados. <strong>Sin embargo, NO "
"podrán editar el formulario en sí </strong> (agregar o eliminar campos, "
"cambiar el título, etc.).</p>\n"
"<p>Para más información, ver <a href='https://docs.framasoft.org/fr/"
"framaforms/fonctionnalites.html#partage-des-r%C3%A9sultats'>nuestra "
"documentación</a>.</p>"

#: sites/all/modules/framaforms/includes/framaforms.node.inc:777
msgid "You can attach up to 5 files to your form."
msgstr "Puede adjuntar hasta 5 archivos a su formulario."

#: sites/all/modules/framaforms/includes/framaforms.node.inc:778
msgid "file(s)"
msgstr "archivo(s)"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:94
msgid "Welcome to Framaforms"
msgstr "Bienvenido a Framaforms"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:104
msgid "Page not found"
msgstr "Página no encontrada"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:112
msgid "Quick form creation"
msgstr "Creación rápida de formularios"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:113
msgid "Create a template"
msgstr "Crear plantilla"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:114
msgid "Validation rules"
msgstr "Reglas de validación"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:115
msgid "Conditional fields"
msgstr "Campos condicionales"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:116
msgid "Emails"
msgstr "Correos"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:117
msgid "Confirmation"
msgstr "Confirmación"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:118
msgid "Downloads"
msgstr "Descargas"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:119
msgid "Limits"
msgstr "Límites"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:137
msgid "You can edit this form by going to this link : "
msgstr "Puedes editar este formulario a través de este vínculo: "

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:138
msgid "To the form"
msgstr "Al formulario"

#: sites/all/modules/framaforms/includes/framaforms.pages.inc:67
msgid "There was an error creating the page %title : %error"
msgstr "Hubo un error al crear la página %title : %error"
