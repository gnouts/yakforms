# French translation of Sub-pathauto (Sub-path URL Aliases) (7.x-1.3)
# Copyright (c) 2012 by the French translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Sub-pathauto (Sub-path URL Aliases) (7.x-1.3)\n"
"POT-Creation-Date: 2012-09-19 18:02+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: French\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>1);\n"

msgid "Disabled"
msgstr "Désactivé"
