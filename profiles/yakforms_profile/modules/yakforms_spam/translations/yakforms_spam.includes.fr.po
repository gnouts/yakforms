# $Id$
#
# LANGUAGE translation of Drupal (sites-all-modules-framaforms_spam-includes)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-10-08 16:35+0200\n"
"PO-Revision-Date: 2020-10-08 16:39+0200\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 2.3\n"
"Last-Translator: \n"
"Language: fr\n"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:107;216;396
msgid "Master"
msgstr "Maître"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:109
msgid "more"
msgstr "plus"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:110;219;399
msgid "Apply"
msgstr "Appliquer"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:111;220;400
msgid "Reset"
msgstr "Réinitialiser"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:112;221;401
msgid "Order by"
msgstr "Trier par"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:113;222;402
msgid "Asc"
msgstr "Asc"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:114;223;403
msgid "Desc"
msgstr "Desc"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:115;224;404
msgid "Elements per page"
msgstr "Éléments par page"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:116;225
msgid "- All -"
msgstr "- Tout -"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:117;226;406
msgid "Offset"
msgstr "Décalage"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:118;227;407
msgid "« first"
msgstr "« premier"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:119;228;408
msgid "‹ previous"
msgstr "< précédent"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:120;229;409
msgid "next ›"
msgstr "suivant >"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:121;230;410
msgid "last »"
msgstr "dernier »"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:122
msgid "Content"
msgstr "Contenu"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:123;412
msgid "- Choose an operation -"
msgstr "- Choisir une opération -"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:124;415
msgid "Nid"
msgstr "Nid"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:125;413
msgid "Title"
msgstr "Titre"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:126
msgid "uid"
msgstr "uid"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:127;236;419
msgid "Page"
msgstr "Page"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:218;398
msgid "plus"
msgstr "plus"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:232
msgid "[nid]"
msgstr "[nid]"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:235
msgid "[uid]"
msgstr "[uid]"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:397
msgid "Suspicious forms"
msgstr "Formulaires suspects"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:405
msgid "- Tout -"
msgstr "- Tout -"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:411
msgid "Contenu"
msgstr "Contenu"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:414
msgid "Publication date"
msgstr "Date de publication"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:417
msgid "Published"
msgstr "Publié"

#: sites/all/modules/framaforms_spam/includes/framaforms_spam.views.inc:418
msgid "Author is not"
msgstr "Auteur n'est pas"
