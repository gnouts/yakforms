# $Id$
#
# LANGUAGE translation of Drupal (sites-all-modules-yakforms-includes)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  sites/all/modules/yakforms/includes/yakforms.admin.inc: n/a
#  sites/all/modules/yakforms/includes/yakforms.block.inc: n/a
#  sites/all/modules/yakforms/includes/yakforms.expiration.inc: n/a
#  sites/all/modules/yakforms/includes/yakforms.node.inc: n/a
#  sites/all/modules/yakforms/includes/yakforms.pages.inc: n/a
#  sites/all/modules/yakforms/includes/yakforms.util.inc: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-11-24 16:43+0100\n"
"PO-Revision-Date: 2020-12-25 18:52+0000\n"
"Last-Translator: x <hardwired1.0@protonmail.com>\n"
"Language-Team: Italian <https://weblate.framasoft.org/projects/yakforms/"
"yakforms-include/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.1\n"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:23
msgid "Total number of forms : @number"
msgstr "Numero totale dei moduli : @number"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:33
msgid "Last @interval : @results"
msgstr "Ultimo @interval : @results"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:46
msgid "Total number of submissions : @number"
msgstr "Numero totale degli invii: @number"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:47
#, fuzzy
msgid "(For an average of @number submissions / form)"
msgstr "(per una media di @number invii / moduli)"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:54
msgid "Total number of users : @number"
msgstr "Numero totale degli utenti: @number"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:64
msgid "Registered since @interval : @number"
msgstr "Registrato da @interval : @number"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:74
msgid "Forms with the most submissions :"
msgstr "Moduli con più invii:"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:86
msgid "submissions"
msgstr "invii"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:87
msgid "Visit"
msgstr "visite"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:92
msgid "<h3>Users with the most forms</h3>"
msgstr "<h3>Utenti con più moduli</h3>"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:113
msgid "Size of database :  @size"
msgstr "Dimensioni della banca dati: @size"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:131
msgid "Note : some of these values can be changed elsewhere on the website, they are simply centralized here."
msgstr ""
"Nota: alcuni di questi valori potrebbero variare altrove sul sito, qui sono "
"soltanto raccolti."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:135
msgid "Website title"
msgstr "Titolo sito web"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:137
msgid "The title of this website. It will be displayed in the top header."
msgstr "Il titolo di questo sito: sarà mostrato nel \"top header\"."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:142
msgid "Website header"
msgstr "Header del sito"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:144
#, fuzzy
msgid "The title that will be displayed at the top of your site. You might use the following HTML tags :"
msgstr ""
"Il titolo che sarà mostrato in testa al tuo sito. Puoi usare queste tag HTML:"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:149
msgid "Website slogan"
msgstr "Slogan del sito"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:151
msgid "Small phrase displayed under the title. Ex : 'Create forms easily'"
msgstr "Frasetta mostrata sotto il titolo. Es: \"crea moduli con facilità\""

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:156
msgid "General contact email"
msgstr "Email di contatto generale"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:158
msgid "It will be used in the general contact form of the website, and used to warn you of pending updates."
msgstr ""
"Sarà usata nel modulo generale di contatto del sito, e usata per avvisare di "
"aggiornamenti in attesa."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:163
msgid "Link to support"
msgstr "Link al supporto"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:165
msgid "Link to a support page for your users. This will be displayed when if your users encounter problems creating forms."
msgstr ""
"Link a una pagina di supporto per i tuoi e le tue utenti. Appare quando "
"l'utente ha un problema nella creazione di un modulo."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:175
msgid "Number of forms per user"
msgstr "Numero di moduli per utente"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:179
msgid "Maximum number of form per user"
msgstr "Masismo numero di moduli per utente"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:181;189
msgid "0 for no limitation."
msgstr "0 per nessuna limitazione."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:187
msgid "Maximum number of submissions per form"
msgstr "Numero massimo di invii per modulo"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:195
msgid "Forms expiration"
msgstr "Scadenza moduli"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:199
msgid "Expiration period (in weeks)"
msgstr "Periodo di scadenza (in settimane)"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:201
msgid "When a user creates a form, its expiration date will be set by default at this interval in the futur. Ex : if a user creates a form on 01/01/2020 and the expiration value is set to '5', the form will expire by default on 02/05/2020. Insert '0' if you don't want forms to expire."
msgstr ""
"Quando l'utente crea un modulo, la data di scadenza verrà impostata "
"automaticamente in questo intervallo anche in futuro. Es: l'utente crea un "
"modulo l'1/1/2020 e il valore della scandenza è su \"5\", il modulo scadrà "
"in automatico il 02/05/2020. Inserire \"0\" perché non scada."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:207
msgid "Deletion period (in weeks)"
msgstr "Periodo di cancellazione (settimane)"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:209
msgid "Interval of time between the form expiration and its deletion from the database. Insert '0' if you don't want forms to be deleted."
msgstr ""
"Lasso di tempo tra la scadenza del modulo e la sua cancellazione dalla banca "
"dati. Insere \"0\" perché non scada."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:215
msgid "Save form submissions before deleting"
msgstr "Salva gli invii del modulo prima della cancellazione"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:217
msgid "Check this box if you want all form submissions to be saved before deletion. This can be useful for user support."
msgstr ""
"Seleziona questa casella se desideri che tutti gli invii di moduli siano "
"salvati prima della cancellazione. Può tornar utile per dare supporto "
"all'utente."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:229;236
msgid "Notification email"
msgstr "Mail di notifica"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:230
msgid "The subject of notification emails. You can use the following tokens : <strong>[node:title]</strong>."
msgstr ""
"L'oggetto della mail di notifica. Si possono usare questi token: "
"<strong>[node:title]</strong>."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:237
msgid "Here you can change the content of the email that will be sent to the user warning them of expired forms. <i>You can use the following tokens that will be replaced : <strong>[node:url]</strong> (the form URL), <strong>[node:title]</strong> (the form's title), <strong>[yakforms-form:expiration-date]</strong> (the form's expiration date), <strong>[yakforms-form:deletion-date]</strong> (the form's date of deletion).</i>"
msgstr ""
"Qui è possibile cambiare il contenuto dell'email che sarà inviato all'utente "
"per avvertirlo di moduli scaduti. <i> Si può usare questo token, che verrà "
"sostituito <strong>[node:url]</strong> (URL del modulo), "
"<strong>[node:title]</strong> (titolo del modulo), <strong>[yakforms-form"
":expiration-date]</strong> (data di scadenza del modulo), <strong"
">[yakforms-form:deletion-date]</strong> (data di cancellazione del "
"modulo).</i>"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:243
msgid "Notification 'from' email address"
msgstr "Notifica \"da\" indirizzo email"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:244
msgid "The default 'from' address set in the email header."
msgstr "L'indirizzo mail \"da\" di default nell'intestazione della mail."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:256
msgid "Generate default pages"
msgstr "Genera pagine di default"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:262
msgid "By clicking this button, you will create default pages for your website : homepage, 404/403 error pages, etc. You can modify them afterwards."
msgstr ""
"Cliccando su questo pulsante creerai pagine di default per il tuo sito: "
"homepage, pagine di errore 404/403, ecc. Sarà possibile in seguito "
"modificarle."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:266
msgid "Reset all"
msgstr "Annulla tutto"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:272
msgid "Reset all above variables to their initial value."
msgstr "Resetta tutte le variabili ai valori iniziali."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:276
msgid "Submit"
msgstr "Inviare"

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:288
msgid "The default pages were created."
msgstr "Le pagine di default sono state create."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:297
msgid "All global variables for Yakforms were reset."
msgstr "Tutti le variabili globali per Yakforms sono state resettate."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:312
msgid "The following directory doesn't exist or is not writable : !path."
msgstr "Questa directotry non esiste o non può esser scritta: !path."

#: sites/all/modules/yakforms/includes/yakforms.admin.inc:333
msgid "The form was submitted !"
msgstr "Modulo inviato!"

#: sites/all/modules/yakforms/includes/yakforms.block.inc:33
msgid "Contact the author of this form"
msgstr "Contattare l'autore del modulo"

#: sites/all/modules/yakforms/includes/yakforms.block.inc:34
msgid "To contact the author of this form, <a href='@link'> click here</a>"
msgstr "Per contattare l'autore del modulo, <a href='@link'> clicca qui</a>"

#: sites/all/modules/yakforms/includes/yakforms.block.inc:39
msgid "Error in the contact form block (modules/yakforms/includes/yakforms.block.inc) : %error"
msgstr ""
"Errore in un blocco del modulo di contatto (modules/yakforms/includes/"
"yakforms.block.inc) : %error"

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:41
msgid "There was an error inserting expired forms into yakforms_expired : %error"
msgstr ""
"C'è stato un errore nell'inserimento di moduli scaduti in yakforms_expired "
": %error"

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:58
msgid "Notifying users of their expired forms."
msgstr ""

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:72
msgid "Error while trying to notify the user : the user's email could be badly configured."
msgstr ""
"Errore nel tentativo di notifica di un o una utente: la mail utente può "
"esser stata configurata male."

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:74
msgid "Notified all users of their expired forms."
msgstr "Tutti gli e le utenti hanno avuto notifica dei loro moduli scaduti."

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:78
msgid "There was an error notifying users : %error"
msgstr "C'è stato un errore nella notifica agli e alle utenti: %error"

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:101
msgid "Invalid email for user %uid"
msgstr "Email utente non valida %uid"

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:137
msgid "Removing forms from the yakforms_expired table after their expiration date was pushed back."
msgstr ""
"Rimozione dei moduli dalla tabella yakforms_expired dopo che la data di "
"scadenza è stata modificata."

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:162
msgid "Error updating modified nodes : %error"
msgstr "Errore nell'aggiornamento dei nodi modificati: %error"

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:188
msgid "Deleting forms after notification."
msgstr "Cancellazione dei moduli dopo la notifica."

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:211
msgid "The following forms were deleted : %nodes"
msgstr "I moduli seguenti sono stati cancellati: %nodes"

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:214
msgid "There is no form to delete."
msgstr "Non ci sono moduli da cancellare."

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:218
msgid "Error deleting expired nodes : %error"
msgstr "Errore nella cancellazione dei nodi scaduti: %error"

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:238
msgid "Node !nid was not found or could not be exported."
msgstr "Il nodo !nid non è stato trovato o non ha potuto essere esportato."

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:278
msgid "Submissions for node !nid exported."
msgstr "Invio per il nodo !nid esportato."

#: sites/all/modules/yakforms/includes/yakforms.expiration.inc:300
msgid "Unable to clean IPs for node %id : Error in _clean_submissions_ips. %error"
msgstr ""
"Impossibile pulire gli IPs per il nodo %id :Error in _clean_submissions_ips. "
"%error"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:745
#, fuzzy
msgid "<p>If you select 'yes', all results for this form will be accessible <strong>by everyone</strong>.</p> Even if no one but you will be able to edit the results, this is <strong>NOT RECOMMANDED</strong> because you might make public private information of people who would not want it to be public (first and last names, email and postal addresses, telephone numbers...)</p>\n<p><strong>By selecting 'yes', you agree to protect the anonymity of the participants, and will be wholly responsible for information you make public.</strong></p>\n<p>Please note that the 'Users having access to results' option below allows you to give access to the results (read, modify and delete) to selected authenticated users.</p>"
msgstr ""
"<p> selezionando 'sì' tutti i risultati di questo modulo saranno accessibili "
"<strong> da tutti</strong>.</p> Benché ciò non permetta ad altri di editare "
"i risultati, <strong> SCONSIGLIAMO QUESTA OPZIONE</strong> perché si rischia "
"di rendere pubbliche delle informazioni relative a persone che non "
"desiderano renderle pubbliche (nomi e cognomi, indirizzi email e postali, "
"numeri di telefono, ecc.)</p>\n"
"<p><strong> Selezionando 'sì' vi fate carico di proteggere l'anonimato dei "
"partecipanti, e sarete pienamente responsabili delle informazioni che "
"renderete pubbliche.</strong></p>\n"
"<p> che l'opzione 'Gli e le utenti hanno accesso ai risultati' qui sotto vi "
"consente di dare accesso ai risultati (lettura, modifica e cancellazione) a "
"determinati e determinate utenti che hanno effettuato l'accesso. </p>"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:748
msgid "Public access to results"
msgstr "Accedsso pubblco ai risultati"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:749
msgid "Body"
msgstr "Corpo"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:750
msgid "Comment"
msgstr "Commento"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:752
msgid "Description"
msgstr "Descrizione"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:753
msgid "Template description"
msgstr "Descrizione del modello"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:754
msgid "Describe what your form is about. This will appear above your form.<br/>\nIf you want to display pictures, you should consider using an external image hosting service, like <a href='https://framapic.org' target='_blank'>framapic.org</a>."
msgstr ""
"Descrivi cosa riguarda il tuo modulo; la descrizione apparirà sopra il "
"modulo. <br/>\n"
"Se vuoi inserire immagini, caldeggiamo l'utilizzo di un servizio di hosting "
"di immagini, come <a href='https://framapic.org' target='_blank'>framapic."
"org</a>."

#: sites/all/modules/yakforms/includes/yakforms.node.inc:756
msgid "Attached files"
msgstr "File allegati"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:757
msgid "Image"
msgstr "Immagine"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:758
msgid "Optional banner"
msgstr "Banner opzionale"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:759
#, fuzzy
msgid "Optional image that will be displayed at the top of your form (example : logo or banner).<br/>\nAfter having selected it by clicking 'Browse', don't forget to click 'Upload' to make it available online."
msgstr ""
"Immagini opzionali che saranno mostrate in testa al modulo (esempio: logo o "
"banner). <br/>\n"
"Dopo averle selezionate, cliccando 'sfoglia', non dimenticate di cliccare "
"'carica' per renderle disponibili in rete."

#: sites/all/modules/yakforms/includes/yakforms.node.inc:761
#, fuzzy
msgid "Forms expire after 6 months, and are destroyed after 9 months (this cover 90% of use cases, and avoids database overloads).<br>\n  At the given date, your form won't be accessible publically. You and you only will however be able to access it - and its results - for 60 more days, from your account.\n  If you wish to extend its expiration date, you simply have to edit the field above once your form is live.<br>\n  If this constraint doesn't suit your needs or if you want more information, please refer to <a href='/limitations'>the limitation page</a>.\n  "
msgstr ""
"I moduli scadono dopo 6 mesi, e sono distrutti dopo 9 (con ciò si copre il "
"90% dei casi, e si evita il sovraccarico delle banche dati). <br/>\n"
"  A una certa data un modulo non sarà più accessibile pubblicamente. Tu e "
"solo tu sarai comunque in grado di accedere a esso e ai risultati per più di "
"60 giorni dal tuo account.\n"
"  Per rinviare la data di scadenza si deve semplicemente modificare il campo "
"qui sopra, una volta che modulo è disponibile. <br/>\n"
"  Se questa limitazione è problematica, o se si desiderano ulteriori "
"informazioni, invitiamo a consultare <a href='/limitations'>la pagina delle "
"limitazioni</a>.\n"
"  "

#: sites/all/modules/yakforms/includes/yakforms.node.inc:766
#, fuzzy
msgid "List this form as a template"
msgstr "Qualifica questo modulo \"modello\""

#: sites/all/modules/yakforms/includes/yakforms.node.inc:767
msgid "A short description of your form template (for example : « Subscription form for an concert »)."
msgstr ""
"Una breve descrizione del tuo modello di modulo (per esempio: \"modulo di "
"sottoscrizione per un concerto\")."

#: sites/all/modules/yakforms/includes/yakforms.node.inc:768
msgid "Words separated by commas."
msgstr "Parole separate da virgole."

#: sites/all/modules/yakforms/includes/yakforms.node.inc:769
msgid "If you check this box, you form will be displayed in the public list of templates, allowing anybody to clone it, use it as a form template and adapt it.<strong>If it is listed as a template, any visitor will be able to fill in your form.</strong>."
msgstr ""
"Selezionando questa casella, il modulo sarà mostrato nella lista pubblica "
"dei modelli di modulo, permettendo a chiunque di clonarlo, usarlo come "
"modello di modulo e adattarlo. <strong>Se è qualificato come modello "
"qualsiasi visitatore potrà compilarlo. </strong>"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:770
msgid "Tags"
msgstr "Tag"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:771
msgid "Upload an image to display with this content."
msgstr "Carica un'immagine da mostrare con questo contenuto."

#: sites/all/modules/yakforms/includes/yakforms.node.inc:772
msgid "Users having access to the results."
msgstr "Gli e le Utenti hanno accesso ai risultati."

#: sites/all/modules/yakforms/includes/yakforms.node.inc:773
msgid "<p>You can authorize other authenticated users to access this form's results : this requires them already having created an account. To do that, fill in their usernames in the field above (separated by commas if several).</p>\n  <p>When logged in, these users will have access to the results by browsing to the form's page, and clicking on the 'Results' tab. They will be able to edit and delete results. <strong>They will however NOT be able to edit the form itself</strong> (add or remove fields, change the title, etc).</p>\n  <p>For more information, see <a href='https://docs.yakforms.org/features/partage-resultats/'>our documentation</a>.</p>"
msgstr ""
"<p>È possibile autorizzare altri e altre utenti, che abbiano effettuato "
"l'accesso, a accedere ai risultati; è necessario che abbiano creato un "
"account. Per far ciò devono inserire il loro nome utente nel campo qui sopra "
"(separati da virgole se più di uno).</p>\n"
"  <p> Se hanno acceduto, questi e queste utenti avranno accesso ai risultati "
"recandosi sul modulo, e cliccando sul pulsante 'Risultati'. Ppotranno "
"editare e cancellare i risultati. <strong> Tuttavia NON potranno editare la "
"struttura del modulo.</strong> (aggiungere o rimuovere campi, cambiare il "
"titolo, ecc.). </p>\n"
"  <p>Per ulteriori informazioni, vedere <a href='https://docs.yakforms.org/features/partage-resultats/'>our "
"documentation</a>.</p>"

#: sites/all/modules/yakforms/includes/yakforms.node.inc:776
msgid "You can attach up to 5 files to your form."
msgstr "Puoi allegare fino a 5 file al modulo."

#: sites/all/modules/yakforms/includes/yakforms.node.inc:777
msgid "file(s)"
msgstr "File"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:101
msgid "Welcome to Yakforms"
msgstr "Benvenuti su Yakforms"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:107
msgid "Access denied"
msgstr "Accesso negato"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:111
msgid "Page not found"
msgstr "Pagina non trovata"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:119
msgid "Quick form creation"
msgstr "Creazione rapida di forma"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:120
msgid "Create a template"
msgstr "Creare un modello"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:121
msgid "Validation rules"
msgstr "Regole di convalida"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:122
msgid "Conditional fields"
msgstr "Campi condizionali"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:123
msgid "Emails"
msgstr "Email"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:124
msgid "Confirmation"
msgstr "Conferma"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:125
msgid "Downloads"
msgstr "Download"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:126
msgid "Limits"
msgstr "Limiti"

#: sites/all/modules/yakforms/includes/yakforms.pages.inc:69
msgid "There was an error creating the page %title : %error"
msgstr "C'è stato un errore nella creazione della pagina %title : %error"

#: sites/all/modules/yakforms/includes/yakforms.util.inc:34
msgid "Error getting user's forms : !error"
msgstr "Errore nel reperimento del modulo dell'utente: !error"

#: sites/all/modules/yakforms/includes/yakforms.util.inc:78
msgid "Warning : node !nid doesn't seem to have an expiration date, but it was demanded."
msgstr ""
"Attenzione : il nodo !nid non ha una data di scadenza, e tuttavia una data è "
"richiesta."

#: sites/all/modules/yakforms/includes/yakforms.util.inc:87
msgid "Error getting form's expiration date : !error"
msgstr "Errore nel reperimento della scadenza del modulo: !error"

#: sites/all/modules/yakforms/includes/yakforms.util.inc:112
msgid "Error getting form's submissions : !error"
msgstr "Errore nel reperimento degli invii del modulo : !error"
