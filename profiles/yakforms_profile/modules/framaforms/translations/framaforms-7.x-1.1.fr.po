# $Id$
#
# LANGUAGE translation of Drupal (sites-all-modules-framaforms)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  sites/all/modules/framaforms/framaforms.module: n/a
#  sites/all/modules/framaforms/framaforms.install: n/a
#  sites/all/modules/framaforms/framaforms.info: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2020-10-08 16:15+0200\n"
"PO-Revision-Date: 2020-12-15 23:52+0000\n"
"Last-Translator: Govi <govi@live.fr>\n"
"Language-Team: French <https://weblate.framasoft.org/projects/framaforms/"
"framaforms/fr/>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.1\n"

#: sites/all/modules/framaforms/framaforms.module:91
msgid ""
"Your account has reached this site's maximum number of forms. Please delete "
"some of your unused forms in 'My forms' before trying to create a new one."
msgstr ""
"Votre compte a atteint le nombre maximum de formulaire autorisé par ce site. "
"Merci de supprimer vos formulaires inutilisés dans le menus \"Mes formulaires"
"\" avant d'en créer un nouveau."

#: sites/all/modules/framaforms/framaforms.module:92
msgid ""
"This site's allows to create the maximum following number of forms : @number"
msgstr "Nombre maximum de formulaires autorisés par ce site : @number"

#: sites/all/modules/framaforms/framaforms.module:106
msgid "Contact form link"
msgstr "Lien vers le formulaire de contact"

#: sites/all/modules/framaforms/framaforms.module:116
msgid "Password alert"
msgstr "Alerte mot de passe"

#: sites/all/modules/framaforms/framaforms.module:126
msgid "General header"
msgstr "En-tête général"

#: sites/all/modules/framaforms/framaforms.module:134
msgid "User links"
msgstr "Liens utilisateur⋅ices"

#: sites/all/modules/framaforms/framaforms.module:144
msgid "Anonymous user links"
msgstr "Lien utilisateur⋅ices anonyme"

#: sites/all/modules/framaforms/framaforms.module:154
msgid "New form warning"
msgstr "Avertissement nouveau formulaire"

#: sites/all/modules/framaforms/framaforms.module:155;221
msgid "New form"
msgstr "Nouveau formulaire"

#: sites/all/modules/framaforms/framaforms.module:165;166
msgid "My Framaforms account"
msgstr "Mon compte Framaforms"

#: sites/all/modules/framaforms/framaforms.module:175
msgid "Help : confirmation email"
msgstr "Aide : mail de confirmation"

#: sites/all/modules/framaforms/framaforms.module:176
msgid "Confirmation email"
msgstr "Mail de confirmation"

#: sites/all/modules/framaforms/framaforms.module:184
msgid "Help : email interaction"
msgstr "Aide : interaction par email"

#: sites/all/modules/framaforms/framaforms.module:185
msgid "Email interaction"
msgstr "Interaction par email"

#: sites/all/modules/framaforms/framaforms.module:193
msgid "Help : validation conditions"
msgstr "Aide : conditions de validation"

#: sites/all/modules/framaforms/framaforms.module:194
msgid "Validation conditions"
msgstr "Conditions de validation"

#: sites/all/modules/framaforms/framaforms.module:202
msgid "Help : configure form"
msgstr "Aide : configuration du formulaire"

#: sites/all/modules/framaforms/framaforms.module:203
msgid "Configure form"
msgstr "Configuration du formulaire"

#: sites/all/modules/framaforms/framaforms.module:211
msgid "Help : conditionnal fields"
msgstr "Aide : champs conditionnels"

#: sites/all/modules/framaforms/framaforms.module:212
msgid "Conditionnal fields"
msgstr "Champs conditionnels"

#: sites/all/modules/framaforms/framaforms.module:220
msgid "Help : new form"
msgstr "Aide : nouveau formulaire"

#: sites/all/modules/framaforms/framaforms.module:229
msgid "Help : global"
msgstr "Aide : global"

#: sites/all/modules/framaforms/framaforms.module:230
msgid "Help"
msgstr "Aide"

#: sites/all/modules/framaforms/framaforms.module:238
msgid "Help : new form creation - webform"
msgstr "Aide : création d'un nouveau formulaire - webform"

#: sites/all/modules/framaforms/framaforms.module:239
msgid "New form configuration"
msgstr "Configuration d'un nouveau formulaire"

#: sites/all/modules/framaforms/framaforms.module:247
msgid "Help : add form components"
msgstr "Aide : ajouter des composants de formulaire"

#: sites/all/modules/framaforms/framaforms.module:248
msgid "Add form components"
msgstr "Ajouter des composants de formulaire"

#: sites/all/modules/framaforms/framaforms.module:256
msgid "Footer"
msgstr "Pied de page"

#: sites/all/modules/framaforms/framaforms.module:257
msgid "Powered by Framaforms"
msgstr "Propulsé par Framaforms"

#: sites/all/modules/framaforms/framaforms.module:318
msgid "Do not communicate any password through Framaforms."
msgstr "Ne communiquez aucun mot de passe via Framaforms."

#: sites/all/modules/framaforms/framaforms.module:333
msgid "Register"
msgstr "Créer un compte"

#: sites/all/modules/framaforms/framaforms.module:335
msgid "Log in"
msgstr "Connexion"

#: sites/all/modules/framaforms/framaforms.module:349
msgid "My account"
msgstr "Mon compte"

#: sites/all/modules/framaforms/framaforms.module:352
msgid "Log out"
msgstr "Déconnexion"

#: sites/all/modules/framaforms/framaforms.module:356
msgid "Create a new form"
msgstr "Créer un formulaire vierge"

#: sites/all/modules/framaforms/framaforms.module:358
msgid "Create a form from a template"
msgstr "Créer un formulaire à partir d'un modèle"

#: sites/all/modules/framaforms/framaforms.module:360
msgid "My forms"
msgstr "Mes formulaires"

#: sites/all/modules/framaforms/framaforms.module:371
msgid "Welcome to your Framaforms account !"
msgstr "Bienvenue sur votre compte Framaforms !"

#: sites/all/modules/framaforms/framaforms.module:372
msgid "You can :"
msgstr "Vous pouvez :"

#: sites/all/modules/framaforms/framaforms.module:374
msgid "edit your personal information (\"Edit\" tab above) ;"
msgstr ""
"modifier vos informations personnelles (onglet \"Modifier\" ci-dessus) ;"

#: sites/all/modules/framaforms/framaforms.module:375
msgid "access your forms (\"My forms\" button above) ;"
msgstr "accéder à vos formulaires (bouton \"Mes formulaires\" ci-dessus) ;"

#: sites/all/modules/framaforms/framaforms.module:376
msgid "create a new form"
msgstr "créer un nouveau formulaire"

#: sites/all/modules/framaforms/framaforms.module:377
msgid "contact us"
msgstr "nous contacter"

#: sites/all/modules/framaforms/framaforms.module:383
msgid "You are about to create a new form."
msgstr "Vous aller créer un nouveau formulaire."

#: sites/all/modules/framaforms/framaforms.module:384
msgid "Start by filling the fields below, then click « Submit »"
msgstr ""
"Commencez par remplir les champs ci-dessous, puis cliquer sur \"Enregistrer\""

#: sites/all/modules/framaforms/framaforms.module:385
msgid ""
"NB : the form's title will affect its URL. Choose it wisely and watch out : "
"changing it will change the form's URL !"
msgstr ""
"NB : le titre du formulaire sera repris dans l'URL. Choisissez-le "
"soigneusement, et attention : toute modification du titre entraînera une "
"modification de l'URL de votre formulaire !"

#: sites/all/modules/framaforms/framaforms.module:393
msgid ""
"Here, you can specify how long you want the confirmation email to be valid "
"(if you are using this option), and if you want the user's submission to be "
"eventually destroyed."
msgstr ""
"Ici, vous pouvez indiquer combien de temps l'email de confirmation sera "
"valable (si vous utilisez cette option), et si vous souhaitez qu'à ce terme "
"la soumission soit détruite."

#: sites/all/modules/framaforms/framaforms.module:395
msgid ""
"(NB : if you are looking for the configuration parameters, you'll find them "
"in the « Emails » tab !)"
msgstr ""
"NB importante : si vous souhaitez utiliser les options de configuration, "
"c'est dans l'onglet « Courriels » que cela ce passe :)"

#: sites/all/modules/framaforms/framaforms.module:402
msgid ""
"This feature allows you to send an email to a specified address.\n"
"        For example you can send an email to the form's author at each new "
"submission.\n"
"        This email can optionally contain the values submitted by the user."
msgstr ""
"Cette fonctionnalité permet, a minima, d'envoyer un courriel à une adresse "
"donnée. \n"
"\tPar exemple pour envoyer, à chaque soumission du formulaire, un courriel "
"au créateur du formulaire, indiquant qu'un formulaire a été soumis. \n"
"\tCe courriel peut contenir les valeurs saisies par le participant."

#: sites/all/modules/framaforms/framaforms.module:405
msgid ""
"But the real interest of this module is to set up a confirmation process by "
"email."
msgstr ""
"Mais surtout, ce module permet de mettre en place une procédure de "
"confirmation par courriel."

#: sites/all/modules/framaforms/framaforms.module:406
msgid ""
"If a user submits a form, they will receive an email with a confirmation "
"link. Their submission won't be appear in the results until they have "
"clicked on this link.\n"
"        This can be very useful if you want to make sure all submitted "
"emails are valid."
msgstr ""
"Ainsi, si l'utilisateur soumet un formulaire, il recevra un courriel avec un "
"lien de validation. Tant qu'il n'aura pas cliqué sur ce lien, son formulaire "
"ne sera pas comptabilisé dans les résultats (il sera enregistré, mais pas "
"validé). Cela peut être très utile dans le cas où vous souhaiteriez vous "
"assurer que tous les participants ont des courriels valides."

#: sites/all/modules/framaforms/framaforms.module:408
msgid ""
"Watch out : to use the confirmation process, your form must have an « email "
"» component."
msgstr ""
"Attention : pour utiliser la confirmation, votre formulaire doit "
"obligatoirement comporter un composant « courriel »."

#: sites/all/modules/framaforms/framaforms.module:409
msgid ""
" (well yeah, how can you possibly send an email if you don't have an "
"address ? :P )"
msgstr ""
" (ben oui, sinon, comment voulez vous qu'on envoie le mail au participant ? :"
"P)"

#: sites/all/modules/framaforms/framaforms.module:416
msgid "(for advanced users)"
msgstr "(réservé plutôt aux utilisateur⋅ices avancé⋅es)"

#: sites/all/modules/framaforms/framaforms.module:417
msgid ""
"Here, you can define conditions to validate a user's submission.\n"
"      If these conditions are not fulfilled, the submission won't be saved, "
"and the user will be asked to modify their submission."
msgstr ""
"Ici, vous pouvez définir des conditions de validation du formulaire en "
"fonction des choix de l'utilisateur⋅ice. \n"
"\tSi ces conditions ne sont pas remplies, le formulaire n'est pas validé. Il "
"est resoumis à l'utilisateur⋅ice jusqu'à correspondre aux conditions."

#: sites/all/modules/framaforms/framaforms.module:419
msgid "Use cases :"
msgstr "Cas d'usages :"

#: sites/all/modules/framaforms/framaforms.module:421
msgid "You want to set the maximum number of characters in a text area to 200."
msgstr ""
"Vous voulez que le nombre maximum de caractères d'une zone de texte soit "
"inférieur à 200 caractères."

#: sites/all/modules/framaforms/framaforms.module:422
msgid "You want to forbid the use of some words in a text area"
msgstr "Vous voulez interdire certains mots dans une zone de texte"

#: sites/all/modules/framaforms/framaforms.module:423
msgid ""
"You want to make sure a 'size at age 5' field is inferior to a 'size at age "
"20' field."
msgstr ""
"Vous voulez comparer que « taille (cm) à 5 ans » est plus petit que « taille "
"(cm) à 20 ans »."

#: sites/all/modules/framaforms/framaforms.module:424
msgid "You want to make sure that 'Awesome project name' begins with 'Frama-'"
msgstr ""
"Vous voulez vous assurer « Nom du projet » commence par les lettres « Frama »"

#: sites/all/modules/framaforms/framaforms.module:425
msgid "etc"
msgstr "etc"

#: sites/all/modules/framaforms/framaforms.module:432
msgid "Getting real, isn't it ? :)"
msgstr "On ne rigole plus là, hein ? :)"

#: sites/all/modules/framaforms/framaforms.module:433
msgid "Here are the advanded parameters for your form."
msgstr "On rentre dans le dur de la configuration avancée du formulaire."

#: sites/all/modules/framaforms/framaforms.module:434
msgid "The good part : The default configuration is enough for 90% of cases."
msgstr "Avantage : la configuration par défaut convient dans 90% des cas."

#: sites/all/modules/framaforms/framaforms.module:435
msgid ""
"The less good part : if you mess around and end up breaking your form, don't "
"come whining ! (Don't worry, you'll 'only' have broken your own form, the "
"worst that can happen is having to start over a new one :)"
msgstr ""
"Inconvénient : si vous touchez à quelques chose qu'il ne fallait pas, ne "
"venez pas vous plaindre (bon, vous ne prenez que le risque de ne casser "
"*que* ce formulaire, alors au pire il suffira d'en recréer un ! :)"

#: sites/all/modules/framaforms/framaforms.module:442
msgid ""
"Conditonal fields allow you to take actions depending on the user's values. "
msgstr ""
"Les champs conditionnels vous permettent d'effectuer des actions en fonction "
"de certaines valeurs saisies par l'utilisateur⋅ice. "

#: sites/all/modules/framaforms/framaforms.module:443
msgid "Examples :"
msgstr "Exemples :"

#: sites/all/modules/framaforms/framaforms.module:444
msgid "Show and hide fields :"
msgstr "Afficher ou masquer des champs :"

#: sites/all/modules/framaforms/framaforms.module:445
msgid "IF the user selected « other » for « Structure type » "
msgstr ""
"SI l'utilisateur⋅ice sélectionne le champs « Autre » dans le menu « Types de "
"structure » "

#: sites/all/modules/framaforms/framaforms.module:446
msgid "THEN  show the text area « Other structure types » "
msgstr "ALORS faire apparaître le champ « Autre type de structure » "

#: sites/all/modules/framaforms/framaforms.module:447
msgid "IF the user checked the option « I don't like soup » "
msgstr "SI l'utilisateur⋅ice coche le bouton « Je n'aime pas la purée » "

#: sites/all/modules/framaforms/framaforms.module:448
msgid "THEN hide the question « What's your favorite soup ? »."
msgstr "ALORS cacher le menu « Quelle est votre purée préférée ? »."

#: sites/all/modules/framaforms/framaforms.module:449
msgid "Force a value :"
msgstr "Forcer une valeur :"

#: sites/all/modules/framaforms/framaforms.module:450
msgid ""
"IF the user checked the option « I got 3 presents or more for Christmas » "
msgstr ""
"SI l'utilisateur⋅ice coche la case « J'ai reçu au moins 3 cadeaux à Noël » "

#: sites/all/modules/framaforms/framaforms.module:451
msgid ""
"THEN automatically check the option « Yes » for the question « Am I lucky ? "
"» "
msgstr ""
"ALORS automatiquement cocher la réponse « Oui » pour la case « J'ai de la "
"chance » "

#: sites/all/modules/framaforms/framaforms.module:452
msgid "Make a field mandatory : "
msgstr "Forcer le remplissage d'un champ : "

#: sites/all/modules/framaforms/framaforms.module:453
msgid "IF the user checked the option « I went to space » "
msgstr "SI l'utilisateur⋅ice a coché la case « Je suis allé dans l'espace » "

#: sites/all/modules/framaforms/framaforms.module:454
msgid "THEN make the field « Which planet ? » mandatory. "
msgstr "ALORS rendre le champ « Sur quelle planète ? » olibgatoire. "

#: sites/all/modules/framaforms/framaforms.module:455
msgid "More information"
msgstr "Plus d'informations"

#: sites/all/modules/framaforms/framaforms.module:462
msgid ""
"Fill the general information describing your form, then click « Submit » to "
"proceed to add form fields."
msgstr ""
"Remplissez les informations de description de votre formulaire, puis cliquez "
"sur «enregistrer» pour passer à l'étape de création des champs de "
"formulaires."

#: sites/all/modules/framaforms/framaforms.module:463
msgid ""
"Once this is done, click on the « Webform » tab <strong>which will only "
"appear once the description is saved !</strong>"
msgstr ""
"Une fois validé, cliquez sur l'onglet « Formulaire » <strong>qui "
"n'apparaîtra qu'une fois la description enregistrée !</strong>"

#: sites/all/modules/framaforms/framaforms.module:470
msgid ""
"Cliquez sur l'onglet « Formulaire » pour poursuivre la création de "
"formulaire..."
msgstr ""
"Cliquez sur l'onglet « Formulaire » pour poursuivre la création de "
"formulaire..."

#: sites/all/modules/framaforms/framaforms.module:477
msgid "Here we go !"
msgstr "C'est parti !"

#: sites/all/modules/framaforms/framaforms.module:478
msgid "Choose your form's components."
msgstr "Choisissez les éléments de votre formulaire."

#: sites/all/modules/framaforms/framaforms.module:479
msgid ""
"You can drag and drop the components on the right into the preview zone to "
"build the form you want."
msgstr ""
"Vous pouvez cliquer ou glisser-déplacer les éléments de formulaire situés à "
"droite dans la zone de prévisualisation pour construire votre formulaire."

#: sites/all/modules/framaforms/framaforms.module:480
msgid ""
"You can also set advanced parameters for your form by clicking on « Emails "
"», « Conditional fields », etc ."
msgstr ""
"Par ailleurs, vous pouvez sélectionner les options avancées en utilisant les "
"sous-onglets «Champs conditionnels», «Courriels», «Paramètres du "
"formulaire», etc."

#: sites/all/modules/framaforms/framaforms.module:481
msgid ""
"Watch out : it is not recommanded to modify your form once you received at "
"least one submission You might loose your data."
msgstr ""
"Attention toutefois : il est déconseillé de modifier un formulaire (modifier "
"son titre ou supprimer des champs) une fois que vous aurez obtenu vos "
"premières réponses. Vous risqueriez de perdre des données."

#: sites/all/modules/framaforms/framaforms.module:482
msgid ""
"You must insert at least ONE form field before clicking « Submit ». If your "
"form remains empty, it will be considered as a potential spam, and you might "
"loose your work."
msgstr ""
"Vous devez insérer au minimum un champ de formulaire avant de le valider. Si "
"votre formulaire reste vide, il sera considéré comme un spam potentiel, et "
"vous risquez de perdre votre travail."

#: sites/all/modules/framaforms/framaforms.module:489
msgid ""
"Here you can define the same components as with the graphic menu, but in a "
"more... « manual » way. "
msgstr ""
"Ici, vous pouvez définir les même composants qu'avec l'interface graphique, "
"mais de manière plus... « manuelle ». "

#: sites/all/modules/framaforms/framaforms.module:490
msgid "This might work better on mobile."
msgstr ""
"L'édition des composants de cette manière peut mieux fonctionner sur mobile."

#: sites/all/modules/framaforms/framaforms.module:494
msgid ""
" is an online, free, add-less and privacy-friendly service to create forms "
"(polls, surveys, quiz, etc.) "
msgstr ""
" est un service en ligne libre, gratuit, sans publicité et respectueux de "
"vos données permettant de générer des formulaires (sondages, enquêtes, "
"pétitions, etc.) "

#: sites/all/modules/framaforms/framaforms.module:495
msgid "This service (and a whole lot of others) are brought to you by"
msgstr "Ce service (et bien d'autres) vous est proposé par"

#: sites/all/modules/framaforms/framaforms.module:495
msgid "as part of "
msgstr "dans le cadre de "

#: sites/all/modules/framaforms/framaforms.module:495
msgid "the De-google-ify Internet campaign"
msgstr "sa campagne « Dégooglisons Internet »"

#: sites/all/modules/framaforms/framaforms.module:496
msgid "This content was not written nor approved by Framasoft."
msgstr "Ce contenu n'est ni rédigé, ni cautionné par Framasoft."

#: sites/all/modules/framaforms/framaforms.module:496
msgid "Report an abusive content."
msgstr "Signaler un cas d'utilisation abusive."

#: sites/all/modules/framaforms/framaforms.module:497
msgid "Framaforms is powered by"
msgstr "Framaforms est propulsé par"

#: sites/all/modules/framaforms/framaforms.module:564
msgid "Current user is author of the node"
msgstr "L'utilisateur est l'auteur du nœud"

#: sites/all/modules/framaforms/framaforms.module:628
msgid "Cron task launched."
msgstr "Tâche cron lancée."

#: sites/all/modules/framaforms/framaforms.module:16
msgid "Configure Framaforms"
msgstr "Configurer Framaforms"

#: sites/all/modules/framaforms/framaforms.module:17
msgid "Configuration for Framaforms module"
msgstr "Configuration pour le module Framaforms"

#: sites/all/modules/framaforms/framaforms.module:25
msgid "Overview"
msgstr "Vue d'ensemble"

#: sites/all/modules/framaforms/framaforms.module:26
msgid "General dashboard containing statistics about your website."
msgstr ""
"Panneau d'administration général affichant des statistiques générales sur "
"votre site."

#: sites/all/modules/framaforms/framaforms.module:34
msgid "Expiration"
msgstr "Expiration"

#: sites/all/modules/framaforms/framaforms.module:35
msgid "Instructions page for expired forms."
msgstr "Page d'instructions pour les formulaires expirés."

#: sites/all/modules/framaforms/framaforms.module:43
msgid "Instructions to share the form."
msgstr "Instructions pour partager le formulaire."

#: sites/all/modules/framaforms/framaforms.install:70
msgid "Framaforms : your form « [form_title] » will expire soon."
msgstr "Framaforms : Votre formulaire « [form_title] » expirera bientôt."

#: sites/all/modules/framaforms/framaforms.info:0
msgid "Framaforms"
msgstr "Framaforms"

#: sites/all/modules/framaforms/framaforms.info:0
msgid "Framaforms functions for creating, managing and sharing webforms."
msgstr "Fonction Framaforms pour créer, gérer et partager des formulaires."

#~ msgid "Features"
#~ msgstr "Fonctionnalités"

#~ msgid "Share"
#~ msgstr "Partager"

#~ msgid "framaforms"
#~ msgstr "framaforms"
